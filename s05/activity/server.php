<?php
session_start();

class User {
    public function login($username, $password) {
        // Check if the provided credentials are correct
        if ($username === 'johnsmith@gmail.com' && $password === '1234') {
            // Set the email in the session
            $_SESSION['email'] = $username;
        } else {
            // Set an error message in the session
            $_SESSION['login_error_message'] = 'Incorrect username or password';
        }
    }

    public function logout() {
        // Clear user information and session
        unset($_SESSION['email']);
        session_destroy();
    }
}

// Handle form submission and logout action
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $user = new User();

    if (isset($_POST['username']) && isset($_POST['password'])) {
        $user->login($_POST['username'], $_POST['password']);
    }

    header('Location: index.php');
} elseif (isset($_GET['action']) && $_GET['action'] === 'logout') {
    $user = new User();
    $user->logout();
    header('Location: index.php');
}
?>
