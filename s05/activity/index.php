<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S05 Activity</title>
	</head>
	<body>
		
	    <form method="POST" action="server.php">
	        <label for="username">Username:</label>
	        <input type="text" id="username" name="username" required>
	        <label for="password">Password:</label>
	        <input type="password" id="password" name="password" required>
	        <button type="submit">Login</button>
    	</form>
    	<!-- Display login error message if it exists -->
    	    <?php
    	    session_start();
    	    if (isset($_SESSION['login_error_message'])) {
    	        echo '<p style="color: red;">' . $_SESSION['login_error_message'] . '</p>';
    	        unset($_SESSION['login_error_message']);
    	    }
    	    ?>
    	    <?php
    	    if (isset($_SESSION['email'])) {
    	        echo '<p>Hello, ' . $_SESSION['email'] . '!</p>';
    	        echo '<form method="POST" action="server.php">';
    	        echo '<input type="hidden" name="logout" value="logout">';
    	        echo '<a href="server.php?action=logout">Logout</a>';

    	        echo '</form>';
    	    }
    	    ?>




	</body>
</html>