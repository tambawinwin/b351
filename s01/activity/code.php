<?php
// Function to get a full address
function getFullAddress($country, $city, $province, $specificAddress) {
    $address = "$specificAddress, $city, $province, $country";
    return $address;
}

// Function to get a letter grade based on a numerical score
function getLetterGrade($score) {
    if ($score >= 98 && $score <= 100) {
        return 'A+';
    } elseif ($score >= 95 && $score <= 97) {
        return 'A';
    } elseif ($score >= 92 && $score <= 94) {
        return 'A-';
    } elseif ($score >= 89 && $score <= 91) {
        return 'B+';
    } elseif ($score >= 86 && $score <= 88) {
        return 'B';
    } elseif ($score >= 83 && $score <= 85) {
        return 'B-';
    } elseif ($score >= 80 && $score <= 82) {
        return 'C+';
    } elseif ($score >= 77 && $score <= 79) {
        return 'C';
    } elseif ($score >= 75 && $score <= 76) {
        return 'C-';
    } else {
        return 'F';
    }
}
?>
